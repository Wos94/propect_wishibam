<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PropectController@index');
Route::get('/search','PropectController@search');
Route::get('/rechercherAdresse','skrappController@RechercherEmail');
Route::resource('prospect','PropectController');
// Export to excel
Route::get('/exportExcel','PropectController@exportExcel');
// Export to csv
Route::get('/exportCSV','PropectController@exportCSV');;

Route::resource('skrapp','skrappController');

@extends('layouts.layout')
@section('content')
@if ($message = Session::get("success"))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif
<div class="row">
    <div class="col-md-10">
        <form action="/rechercherAdresse" method="get">
            <div class="input-group">
                <input type="search" class="form-control" name="prenom" placeholder="Entrez le Prenom">
                <input type="search" class="form-control" name="nom" placeholder="Entrez le Nom">
                <input type="search" class="form-control" name="domaine" placeholder="Entrez le domaine">
                <span class="input-group-prepend">
                    <button type="submit" class="btn btn-primary">Search</button>
                </span>
            </div>
        </form>
        <div class="card">
            @if ($httpcode==404)
            <div class="card">
                <div class="card-header">
                    INFORMATIONS
                </div>
                <div class="card-body">
                    <h5 class="card-title">RECHERCHE ADRESSE</h5>
                    <p class="card-text">Aucune recherche effectée pour l'instant.</p>
                </div>
            </div>
            @else
                <div class="card">
                    <div class="card-header">
                        RESULTAT
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">ADRESSE TROUVEE</h5>
                        <p class="card-text">EMAIL : {{$response["email"]}}</p>
                    </div>
                </div>

            @endif
        </div>
    </div>
</div>
@endsection
</html>
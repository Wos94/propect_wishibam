@extends('layouts.layout')
@section('content')
@if ($message = Session::get("success"))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif
<div class="row">
    <div class="col-md-6">
        <h1 style="font-size: 20px"; >LA LISTE DES PROSPECTS RECUPERES SUR LE NET</h1>
    </div>
    <div class="col-sm-3 ml-5 mb-2">
        <a href="{{ url('/rechercherAdresse') }}" class="btn btn-dark text-light">RECHERCHE ADRESSES</a>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th class="bg-secondary text-light">Prenom</th>
            <th class="bg-secondary text-light">Nom</th>
            <th class="bg-secondary text-light">Email</th>
            <th class="bg-secondary text-light">Nom de domaine</th>
            <th class="bg-secondary text-light">Titre</th>
            <th class="bg-secondary text-light">Entreprise</th>

        </tr>
        <tbody>
        @foreach($response as $responses)
        <tr>
            <td>{{$responses["first_n"]}}</td>
            <td>{{$responses["last_n"]}}</td>
            <td>{{$responses["email"]}}</td>
            <td>{{$responses["domain"]}}</td>
            <td>{{$responses["title"]}}</td>
            <td>{{$responses["company"]}}</td>
        </tr>
        @endforeach
        </tbody>
        </thead>
    </table>
</div>
<div class="mt-3">
    <a href="{{action('PropectController@exportCSV')}}"><button class="btn btn-success">TELECHARGER FORMAT CSV</button></a>
    <a href="{{action('PropectController@exportExcel')}}"><button class="btn btn-success">TELECHARGER FORMAT EXCEL</button></a>
</div>
@endsection
</html>
@extends('layouts.layout')
@section('content')
 @if ($message = Session::get("success"))
            <div class="alert alert-success">
                <p>{{$message}}</p>
            </div>
@endif
            <div class="row">
                <div class="col-md-6">
                    <h1 style="font-size: 30px;">LA LISTE DES PROSPECTS</h1>
                </div>
                    <div class="col-md-4">
                        <form action="/search" method="get">
                            <div class="input-group">
                                <input type="search" class="form-control" name="search" placeholder="Entrez le nom de l'entreprise ...">
                                <span class="input-group-prepend">
                                    <button type="submit" class="btn btn-primary">Search</button>
                                </span>
                            </div>
                        </form>
                    </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="bg-secondary text-light">Prenom</th>
                        <th class="bg-secondary text-light">Nom</th>
                        <th class="bg-secondary text-light">Email</th>
                        <th class="bg-secondary text-light">Nom de domaine</th>
                        <th class="bg-secondary text-light">Titre</th>
                        <th class="bg-secondary text-light">Entreprise</th>

                    </tr>
                    <tbody>
                    @foreach($prospect as $propects)
                    <tr>
                        <td>{{$propects->prenom}}</td>
                        <td>{{$propects->nom}}</td>
                        <td>{{$propects->email}}</td>
                        <td>{{$propects->nom_domaine}}</td>
                        <td>{{$propects->title}}</td>
                        <td>{{$propects->entreprise}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    </thead>
                </table>
            </div>
    <div class="mt-3">
        <a href="{{action('PropectController@exportCSV')}}"><button class="btn btn-success">TELECHARGER FORMAT CSV</button></a>
        <a href="{{action('PropectController@exportExcel')}}"><button class="btn btn-success">TELECHARGER FORMAT EXCEL</button></a>
    </div>
@endsection
</html>
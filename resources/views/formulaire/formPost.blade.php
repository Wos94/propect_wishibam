
<form action="{{action('PropectController@store')}}" method="post">
    @csrf
    <div class="form-group">
        <label for="">Prenom</label>
        <input class="form-control" type="text" name="prenom" id="prenom"/>
    </div>
    <div class="form-group">
        <label for="">Nom</label>
        <input class="form-control type="text" name="nom" />
    </div>
    <div class="form-group">
        <label for="">Email</label>
        <input class="form-control type="text" name="email" />
    </div>
    <div class="form-group">
        <label for="">Nom de domaine</label>
        <input class="form-control type="text" name="nom_domaine"/>
    </div>
    <div class="form-group">
        <label for="">Titre</label>
        <input class="form-control type="text" name="title"/>
    </div>
    <div class="form-group">
        <label for="">Entreprise</label>
        <input class="form-control type="text" name="entreprise"/>
    </div>
    <button class="btn btn-primary" type="submit">Soumettre</button>
</form>
@extends('layouts.layout')
@section('content')
@if ($message = Session::get("success"))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif
    <div class="row">
        <div class="col-md-6 offset-md-3">
            @if ($message = Session::get("danger"))
            <div class="alert alert-danger">
                <strong>Input data failed , please try agin </strong>
            </div>
            @endif

            <form action="{{action('PropectController@store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="">Prenom</label>
                    <input class="form-control" type="text" name="prenom" id="prenom" required/>
                </div>
                <div class="form-group">
                    <label for="">Nom</label>
                    <input class="form-control" type="text" name="nom" required/>
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input class="form-control" type="email" name="email" required />
                </div>
                <div class="form-group">
                    <label for="">Nom de domaine</label>
                    <input class="form-control" type="text" name="nom_domaine" required/>
                </div>
                <div class="form-group">
                    <label for="">Titre</label>
                    <input class="form-control" type="text" name="title" required/>
                </div>
                <div class="form-group">
                    <label for="">Entreprise</label>
                    <input class="form-control" type="text" name="entreprise" required/>
                </div>
                <button class="btn btn-primary" type="submit">Soumettre</button>
            </form>
        </div>
    </div>
@endsection
</html>
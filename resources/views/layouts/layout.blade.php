<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-dark bg-primary">
    <a class="navbar-brand" href="{{action('PropectController@index')}}">PROSPECT-WISHIBAM</a>
    <div >
        <a href="{{action('PropectController@create')}}" class="btn btn-info">FORMULAIRE DES PROSPECTS</a>
        <a href="{{action('skrappController@index')}}" class="btn btn-info">LISTE DES PROSPECTS SUR INTERNET</a>
    </div>
</nav>
<div class="container">
    <p><br/></p>

    @yield("content")
</div>
</body>
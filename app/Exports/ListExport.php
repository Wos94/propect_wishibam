<?php
/**
 * Created by IntelliJ IDEA.
 * User: Souleymane Sow
 * Date: 24/04/2019
 * Time: 20:38
 */

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Propect;

class ListExport implements FromCollection
{
    public function collection()
    {
        return Propect::all();
    }
}
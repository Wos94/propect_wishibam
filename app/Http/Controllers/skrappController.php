<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
class skrappController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // FONCTION QUI RECUPERE LES DONNEES SUR LE NET GRACES à SKRAPP API
    public function index()
    {
        //
        $curl = curl_init();

        curl_setopt_array($curl, array(
            //permet de recuperer les liens de l'API de SKRAPP
            CURLOPT_URL => "https://api.skrapp.io/api/v2/list/594804/data?emailOnly=true",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                //TOKEN GENERE PAR POSTMAN
                "Postman-Token: f4e65e04-d8d6-4577-8d5f-1ec101db8082",
                //LA CLE UNIQUE DE NOUS DONNE L'API SKRAPP ET PERMET L'AUTHENTIFICATION
                "X-Access-Key: 563061135oZ2Bj5W0G2ZDXPxUZIlfO6paBDrstLwiu"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        }
        // RETOURNE LA VUE SKRAPP AVEC LA VARIABLE $reponse EN PARAMETRE ET LE PUIS LE DECODE
        return view('skrapp')->with('response', json_decode($response, true));
    }


    function RechercherEmail(Request $request) {

        $prenom = $request->get('prenom');
        $nom = $request->get('nom');
        $domaine = $request->get('domaine');
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.skrapp.io/api/v2/find?firstName=".$prenom."&lastName=".$nom."&domain=".$domaine."",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Postman-Token: 14e637c5-b998-4dae-b00a-2c4aa5db3d3d",
                "X-Access-Key: 563061135oZ2Bj5W0G2ZDXPxUZIlfO6paBDrstLwiu"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
    }
        return view('rechercherAdresse',["response"=>json_decode($response, true),"httpcode"=>$httpcode]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function exportExcel()
    {
        return Excel::download(new ListExport, 'list.xlsx');
    }
    /**
     * Export to csv
     */

    // fonction qui permet d'exporter les données au format csv
    public function exportCSV()
    {
        return Excel::download(new ListExport, 'list.csv');
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exports\ListExport;
use Maatwebsite\Excel\Facades\Excel;
class PropectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // fonction qui recupére et renvoi tout les données

    public function index()
    {
        //
        $prospect = DB::select("select*from propects");
        return view('index',["prospect"=>$prospect]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    // fonction qui vous renvoi vers la page du formulaire

    public function create()
    {
        //
        return view('prospecter');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // fonction qui permet d'enregistrer les données dans la base de données

    public function store(Request $request)
    {
        //
        $prenom = $request->get('prenom');
        $nom = $request->get('nom');
        $title = $request->get('title');
        $nom_domaine = $request->get('nom_domaine');
        $email = $request->get('email');
        $entreprise= $request->get('entreprise');

        $posts = DB::insert('insert into propects(prenom,nom,email,title,nom_domaine,entreprise) value(?,?,?,?,?,?)',[$prenom,$nom,$email,$title,$nom_domaine,$entreprise]);
        if($posts) {
            $red = redirect('prospect/create')->with("success","Formulaire soumis avec succés !");
        }

        return $red;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // fonction qui permet de faire la recherche

    public function search(Request $request){
        $search = $request->get('search');
        $prospect = DB::table('propects')->where('entreprise',"like",'%'.$search.'%')->paginate(5);
        return view('index',['prospect' => $prospect]);
    }

    // fonction qui permet d'exporter les données au format xlsx

    public function exportExcel()
    {
        return Excel::download(new ListExport, 'list.xlsx');
    }
    /**
     * Export to csv
     */

    // fonction qui permet d'exporter les données au format csv
    public function exportCSV()
    {
        return Excel::download(new ListExport, 'list.csv');
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

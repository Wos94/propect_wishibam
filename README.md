**Comment réuissir à lancer le projet**

1.  Puller le projet 
2.  creer une base de donnée et recuperer le script sql (**prospect.sql**) qui se trouve dans les fichiers
2.  allez dans le dossier du projet et tapez la commande **composer update**
3.  taper la commande **php artisan key:generate** (il va crée un fichier **.env**)
4.  Dans le fichier **.env** (mettez le numéro de port de votre bdd , puis le nom de la bdd , le username et le password)
5.  tapez la commande **php artisan serve** et enjoy

****NB**** :

Le fichier le plus important c'est le **controlleur** pour y acceder 

cliquez sur le dossier **app -> Http -> Controllers -> PropectController.php***

Pour avoir accés au Model 

**App -> Propect.php**


Pour avoir accés au Vue 

**ressources -> views**

Pour avoir accés au Routes 

**Routes -> web.php**



**Si vous rencontrez des problémes veillez m'envoyer un email : rahmanesow94@gmail.com**
